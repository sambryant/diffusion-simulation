import sys
import os, json
import numpy as np
import seaborn
import matplotlib.pyplot as plt
import sim
import progressbar
from sim import Output

MAX_PLOT = 10

def expected_ca_t_values(times, peq, k_on, k_off, all_start_on: bool=False):
  a = peq
  if all_start_on:
    return a + (1 - a) * np.exp(-(k_on+k_off) * times)
  else:
    return (a - a * a) * np.exp(-(k_on + k_off) * times) + a * a

  # # This next line is wrong its just a test
  # return 1/a*(1-a)*(1-a) * np.exp(-(k_on + k_off) * times)

def go(direc, fnames):
  fig, (ax1, ax2) = plt.subplots(1, 2, figsize=(12.0, 6.0))

  for ax in [ax1, ax2]:
    ax.set_xlabel('t')
    ax.set_ylabel(r'$\langle a(t)a(0)\rangle$')

  for i, fname in enumerate(progressbar.progressbar(fnames)):
    output = Output.load('%s/%s' % (direc, fname))
    params = output.params
    peq = params.compute_peq()
    k_on = params.k_on
    k_off = params.k_off
    c0 = peq - peq*peq

    times = params.times
    num_times = len(params.times)

    expected = expected_ca_t_values(times, peq, k_on, k_off, all_start_on=params.all_start_on)

    ca_t = np.zeros(num_times)
    if any(~output.states[0,:]):
      raise Exception('Expected all on')

    for step, time in enumerate(times):
      ca_t[step] = np.average(output.states[step, :])

    ax = i < 10 and ax1 or ax2
    clr = ax.plot(times, ca_t, linestyle='-.')[0].get_color()
    ax.plot(times, expected, color=clr, linewidth=0.5, linestyle='solid')

  fig.savefig('%s/fig.png' % direc)
  plt.show()

def analyze():
  direc = 'test12-all-start-on'
  base_names = []
  for f in os.listdir(direc):
    if f.endswith('output.npz'):
      base_names.append(os.path.basename(f)[0:-11])
  go(direc, base_names)

if __name__ == '__main__':
  analyze()
