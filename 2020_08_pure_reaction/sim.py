from __future__ import annotations
import numpy as np
import os
import random
import progressbar

class Parameters(object):
  n: int
  dt: float
  t_end: float
  num_times: int
  times: np.ndarray # array[time] = float
  # reaction constants
  k_on: float
  k_off: float
  # movement constants
  D: float
  size: float
  all_start_on: bool

  def __init__(self, n: int, dt: float, t_end: float, size: float, D: float, k_on: float, k_off: float, all_start_on: bool=False) -> None:
    self.n = n
    self.dt = dt
    self.t_end = t_end
    self.num_times = int(np.ceil(t_end/dt))
    self.times = np.array([self.dt * i for i in range(self.num_times)])
    self.k_on = k_on
    self.k_off = k_off
    self.D = D
    self.size = size
    self.all_start_on = all_start_on

  @classmethod
  def from_peq(cls, n: int, dt: float, t_end: float, size: float, D: float, peq: float, flip_frac: float=0.001) -> Parameters:
    # We want no more than `flip_frac` particles to flip in each round.
    # What is expected number of flips?
    k_on_1 = (flip_frac / dt) * peq / (1-peq)
    k_on_2 = (flip_frac / dt)
    k_on  = min(k_on_1, k_on_2)
    k_off = k_on * (1-peq)/peq
    return cls(n, dt, t_end, size, D, k_on, k_off)

  def compute_peq(self) -> float:
    return self.k_on/(self.k_on + self.k_off)

  def save(self, base_name: str) -> None:
    os.makedirs(os.path.dirname(base_name), exist_ok=True)
    np.savez('%s_params.npz' % base_name,
      n=self.n,
      dt=self.dt,
      t_end=self.t_end,
      size=self.size,
      D=self.D,
      k_on=self.k_on,
      k_off=self.k_off,
      all_start_on=self.all_start_on)

  @classmethod
  def load(cls, base_name: str) -> Parameters:
    params_d = np.load('%s_params.npz' % base_name)
    return cls(**params_d)

class Output():
  params: Parameters
  save_states: bool
  save_positions: bool
  fraction: np.ndarray # array[time] = float 0..1
  states: np.ndarry # array[time][particle] = boolean
  positions: np.ndarray # array[time][particle] = float
  _ready: bool

  def __init__(self, params: Parameters, save_states: bool=True, save_positions: bool=True):
    self.params = params
    self.save_states = save_states
    self.save_positions = save_positions
    self._ready = False

  def run(self, verbose: bool=True) -> None:
    self.prepare()

    bar = verbose and progressbar.ProgressBar() or progressbar.NullBar()

    # Initialize species state using random assignment weighted by equilibrium value
    k_on, k_off = self.params.k_on, self.params.k_off
    states = np.array([True for _ in range(self.params.n)], dtype=bool)
    if self.params.all_start_on:
      for i in range(self.params.n):
        states[i] = True
    else:
      states[:] = np.random.random(self.params.n) < self.params.compute_peq()
    prob_on  = k_on * self.params.dt
    prob_off = k_off * self.params.dt

    # Initialize particle position
    sqrt_2dt = np.sqrt(2 * self.params.D * self.params.dt)
    positions = np.random.random(self.params.n) * self.params.size

    for step in bar(range(self.params.num_times)):
      self.write(step, positions, states)

      # Compute diffusion movement
      if self.save_positions:
        dx = sqrt_2dt * np.random.normal()
        positions = (positions + dx) % self.params.size # periodic

      # Compute species state transitions
      rng = np.random.random(self.params.n)
      states = (states * (rng > prob_off)) + (~states * (rng < prob_on))

  def prepare(self) -> None:
    self.fraction = np.zeros(self.params.num_times)
    if self.save_positions:
      self.positions = np.zeros((self.params.num_times, self.params.n))
    else:
      self.positions = np.zeros(1)
    if self.save_states:
      self.states = np.zeros((self.params.num_times, self.params.n), np.bool)
    else:
      self.states = np.zeros(1)
    self._ready = True

  def write(self, step: int, positions: np.ndarray, states: np.ndarray) -> None:
    if not self._ready:
      raise Exception('Output.prepare must be called before writing data')
    self.fraction[step] = np.sum(states) / self.params.n
    if self.save_positions:
      self.positions[step] = positions
    if self.save_states:
      self.states[step] = states

  def save(self, base_name: str) -> None:
    os.makedirs(os.path.dirname(base_name), exist_ok=True)
    self.params.save(base_name)
    np.savez('%s_output.npz' % base_name,
      save_states=self.save_states,
      save_positions=self.save_positions,
      positions=self.positions,
      fraction=self.fraction,
      states=self.states)

  @classmethod
  def load(cls, base_name: str) -> Output:
    params = Parameters.load(base_name)
    outputd = np.load('%s_output.npz' % base_name)
    output = Output(params, save_states=outputd['save_states'], save_positions=outputd['save_positions'])
    output.positions = outputd['positions']
    output.fraction = outputd['fraction']
    output.states = outputd['states']
    return output

def run(params: Parameters, fname: str, verbose: bool=True) -> None:
  output = Output(params, save_positions=False)
  output.run(verbose=verbose)
  output.save(fname)
  if verbose:
    print('Saved output %s' % fname)

def compute_reaction_diffusion():
  n = 100000
  t_stop = 1.0
  t_step = 0.001
  base_dir = 'test10'

  def compute_reaction_diffusion_vary_k_on():
    k_ons = np.linspace(0, 2, 51)[1:]
    k_off = 1.0
    for k_on in progressbar.progressbar(k_ons):
      run(Parameters(n, t_step, t_stop, 1.0, 1.0, k_on, k_off), '%s/k_on/run_%.2f' % (base_dir, k_on), verbose=False)

  def compute_reaction_diffusion_vary_k_off():
    k_offs = np.linspace(0, 2, 51)[1:]
    k_on = 1.0
    for k_off in progressbar.progressbar(k_offs):
      run(Parameters(n, t_step, t_stop, 1.0, 1.0, k_on, k_off), '%s/k_off/run_%.2f' % (base_dir, k_off), verbose=False)
    # util.print_progress_bar(10, 10)

  def compute_reaction_diffusion_vary_both():
    ks = np.linspace(0, 2, 51)[1:]
    for k in progressbar.progressbar(ks):
      run(Parameters(n, t_step, t_stop, 1.0, 1.0, k, k), '%s/k_both/run_%.2f' % (base_dir, k), verbose=False)
  print('Computing for varied k_on set..')
  compute_reaction_diffusion_vary_k_on()
  print('Computing for varied k_off set...')
  compute_reaction_diffusion_vary_k_off()
  print('Computing for varied k_on/k_off set...')
  compute_reaction_diffusion_vary_both()

def final_reaction_correlation_test():
  n = 100000
  t_stop = 3.0
  t_step = 0.001
  base_dir = 'test11'
  num_runs = 20

  for i in progressbar.progressbar(range(num_runs)):
    peq = random.random()
    k_on = random.random()
    k_off = k_on/peq - k_on
    run(Parameters(n, t_step, t_stop, 1.0, 1.0, k_on, k_off), '%s/run_peq%.2f_kon%.2f' % (base_dir, peq, k_on), verbose=False)

def all_start_on():
  n = 100000
  t_stop = 3.0
  t_step = 0.001
  base_dir = 'test12-all-start-on'
  num_runs = 20

  for i in progressbar.progressbar(range(num_runs)):
    peq = random.random()
    k_on = random.random()
    k_off = k_on/peq - k_on
    run(
      Parameters(n, t_step, t_stop, 1.0, 1.0, k_on, k_off, all_start_on=True), '%s/run_peq%.2f_kon%.2f' % (base_dir, peq, k_on), verbose=False)

if __name__ == '__main__':
  all_start_on()
  # final_reaction_correlation_test()
  # compute_reaction_diffusion()
  # compute_reaction_diffusion_vary_k_on()
  # compute_reaction_diffusion_vary_k_off()
  # run(Parameters.from_peq(500, 0.05, 1000.0, 1.0, 1.0, 0.2), 'test3/run_0.2')
  # run(Parameters.from_peq(500, 0.05, 1000.0, 1.0, 1.0, 0.4), 'test3/run_0.4')
  # run(Parameters.from_peq(500, 0.05, 1000.0, 1.0, 1.0, 0.6), 'test3/run_0.6')
  # run(Parameters.from_peq(500, 0.05, 1000.0, 1.0, 1.0, 0.8), 'test3/run_0.8')
