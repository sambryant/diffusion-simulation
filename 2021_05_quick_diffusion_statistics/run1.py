import os
import sim

OUTDIR = 'run1'
NUM = 10000
DT = 0.01
T_END = 10.0
ALPHA = 0.1
alpha_a0s = [
  (0.2, 0.2),
  (0.2, 0.7),
  (0.01, 0.2),
  (0.01, 0.7)
]
if not os.path.exists(OUTDIR):
  os.mkdir(OUTDIR)

for (alpha, a0) in alpha_a0s:
  output = '%s/data_alpha=%.2f_a0=%.1f.hdf5' % (OUTDIR, alpha, a0)
  sim.run(output, NUM, DT, T_END, a0, alpha, verbose=True)
