# Quick Diffusion Statistics

This is a very quick simulation that proves that the reaction equation obeys:

```math
   p\left [a(t)=1\,|\, a(0)=1\right] = (1 - a_0)e^{-(k_{on} + k_{off})t} + a_0
```

This relation can also be easily derived by evaluating $`\frac{d}{d t}C^{a}(t)`$. But this is a double check.

**Note** This does the exact same thing as a much earlier code set in this directory (titled `2020_08_pure_reaction`) at the time of this commit. However, it's far more elegant and it uses HDF5.
