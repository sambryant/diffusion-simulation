import progressbar
import numpy as np
import environ
import os
import sys
import matplotlib.pyplot as plt

def count(positions, radius):
  return np.count_nonzero(np.linalg.norm(positions[:,:], axis=1) < radius)

def analyze(jobname, num_trials, radiuses):
  correlation = None

  actual_avg_d_count = 0.0
  for i in progressbar.progressbar(range(num_trials)):
    d = np.load(environ.get_simulation_file(jobname, 'positions', i))

    if correlation is None:
      correlation = np.zeros((len(radiuses), len(d['times'])))

    for j, radius in enumerate(radiuses):
      density = d['num_particles'] / (2 * d['half_size']) ** 2
      area = 3.14159 * radius * radius
      avg_count = density * area

      d_count_0 = count(d['positions'][0], radius) - avg_count
      for k in range(len(d['times'])):
        d_count_1 = count(d['positions'][k], radius) - avg_count
        actual_avg_d_count += d_count_1
        correlation[j][k] += d_count_0 * d_count_1
  correlation /= num_trials
  print('Actual total dcount = %f' % (actual_avg_d_count / (num_trials * len(radiuses))))

  file = environ.get_simulation_file(jobname, 'count_correlations')
  np.savez(
    file,
    correlation=correlation,
    radius=radius,
    times=d['times'],
    D=d['D'],
    end_time=d['end_time'],
    dt=d['dt'],
    radiuses=radiuses,
    num_particles=d['num_particles'],
    half_size=d['half_size'])

def quick_analyze(jobname, num_trials, radius):
  correlation = 0.0

  D = 0.0
  t = 0.0
  for i in progressbar.progressbar(range(num_trials)):
    d = np.load(environ.get_simulation_file(jobname, 'positions', i))
    D = d['D']
    times = d['times']
    t = times[len(times) - 1]
    density = d['num_particles'] / (2 * d['half_size']) ** 2
    area = 3.14159 * radius * radius
    avg_count = density * area
    d_count_1 = count(d['positions'][0], radius) - avg_count
    d_count_2 = count(d['positions'][len(times) - 1], radius) - avg_count
    correlation += d_count_1 * d_count_2
  print('D dt = %.1f' % (D * t))
  return correlation / num_trials

if __name__ == '__main__':
  radiuses = [0.1, 0.32, 1.0, 3.1, 10.0]
  num_steps = np.array([10, 32, 100, 316, 1000])
    # 3162, 10000])
  Dt = 0.3

  fig, ax = plt.subplots(1, 1, figsize=(8, 8))
  correlations = np.zeros((len(radiuses), len(num_steps)))
  for i, r in enumerate(radiuses):
    for j, n_steps in enumerate(num_steps):
      correlations[i][j] = quick_analyze('test_Dt_constant/test_%d' % n_steps, 500, r)

    ax.plot(0.05 * num_steps, correlations[i])
  ax.set_xlim(0.0, 50.0)
  plt.show()
