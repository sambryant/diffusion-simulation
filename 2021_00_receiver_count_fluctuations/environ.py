import os

def get_simulation_dir(jobname: str, create_if_missing: bool=False) -> str:
  d = os.path.join(os.environ['RESEARCH_DATA_DIR'], 'diffusion', jobname)
  if create_if_missing and not os.path.exists(d):
    os.mkdir(d)
  return d

def get_simulation_file(jobname, filename, trial_num=None):
  d = get_simulation_dir(jobname, create_if_missing=True)
  if trial_num is None:
    return os.path.join(d, '%s.npz' % (filename))
  else:
    return os.path.join(d, '%s_trial_%05d.npz' % (filename, trial_num))

def get_plot_file(jobname, filename):
  d = get_simulation_dir(jobname, create_if_missing=True)
  return os.path.join(d, '%s.png' % (filename))
