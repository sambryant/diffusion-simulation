import numpy as np
from typing import List, Tuple

def generate_bounded_particles(number: int, radius: float, bounds: List[Tuple[float, float]]) -> np.ndarray:
  eff_bounds = [[b[0] + 1.05 * radius, b[1] - 1.05 * radius] for b in bounds]
  sizes = [(b[1] - b[0]) for b in eff_bounds]

  positions = np.zeros((number, len(sizes)))
  for dim in range(len(sizes)):
    positions[:,dim] = np.random.random(number) * sizes[dim] + eff_bounds[dim][0]

  return positions

"""
Below generates non-overlapping particles, but is slow...
"""
def generate_sized_particles(number: int, radius: float, bounds: List[Tuple[float, float]]) -> np.ndarray:
  eff_bounds = [[b[0] + 1.05 * radius, b[1] - 1.05 * radius] for b in bounds]
  sizes = np.array([(b[1] - b[0]) for b in eff_bounds])
  ndims = len(sizes)
  offset = np.array([b[0] for b in eff_bounds])
  cutoff = 4 * radius * radius

  def gen_pos():
    return np.random.random(ndims) * sizes + offset

  def check_collision(p1, positions):
    if not len(positions):
      return
    dr = positions - p1
    dr2 = np.einsum('ij,ij->i', dr, dr)
    return any(dr2 < cutoff)

  index = 0
  positions = np.zeros((number, len(sizes)))

  while index < number:
    p1 = gen_pos()
    if not check_collision(p1, positions[0:index]):
      positions[index] = p1
      index += 1

    # has_collision = False
    # for p2 in positions[0:index]:
    #   dr = p2 - p1
    #   if np.dot(dr, dr) < radius*radius:
    #     has_collision = True
    #     break
    # if not has_collision:
    #   positions[index] = p1
    #   index += 1

  return positions
