import time
from parameters import Parameters
from sim_runner import Run

def main():
  jobname = 'new_job1'
  num_trials = 100
  num_particles = 1000
  size = 10.0
  t_end = 5.0
  dt = 0.01
  D = 100 * size / (2 * t_end) # = 1

  params = Parameters(2, size, num_particles, D, dt, t_end, num_trials)
  run = Run(jobname, params)

  t1 = time.time()
  run.go()
  t2 = time.time()
  print('Simulation took %.2f time' % (t2 - t1))

if __name__ == '__main__':
  main()
