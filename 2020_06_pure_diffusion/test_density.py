from parameters import Parameters, Output
from sim import simulate
import time
import numpy as np
import matplotlib.pyplot as plt
import density
from matplotlib.animation import FuncAnimation

def test_time_loop():
  N = 5
  t_end = 100.0
  dt = 0.1
  sigma2 = 1.0
  N_DENSITY_PTS = 100
  params = Parameters(1, 100.0, N, 10.0, dt, t_end)

  print('Running simulation with %d particles over %d steps' % (N, params.n_steps))
  t0 = time.time()
  output = simulate(params)
  t1 = time.time()

  print('Generating density field with %d points' % (N_DENSITY_PTS))
  points, densities = density.generate_density_field(params, output, N_DENSITY_PTS, sigma2, time_loop=True)
  t2 = time.time()

  print('Sim time: %.2f s' % (t1-t0))
  print('Den time: %.2f s' % (t2-t1))

  fig, ax2 = plt.subplots(1, 1)

  normalization = params.size / (params.n_particles * np.sqrt(2*np.pi*sigma2))

  ax2.set_ylim(0, 5 * normalization)
  ax2.set_xlim(0, params.size)
  ln, = ax2.plot(points, densities[0])

  def update(frame):
    ln.set_ydata(densities[frame,:])
    return ln,

  ani = FuncAnimation(fig, update, frames=params.n_steps, blit=True)
  plt.show()
  # plt.show()

if __name__ == '__main__':
  test_time_loop()
