import os

def get_sim_job_dir(jobname: str, create_if_missing: bool=True) -> str:
  jobdir = os.path.join(os.environ['RESEARCH_JOBS_DIR'], 'diffusion', jobname)
  if create_if_missing and not os.path.exists(jobdir):
    os.mkdir(jobdir)
  return jobdir

def get_sim_job_file(jobname: str, batch_index: int) -> str:
  jobdir = get_sim_job_dir(jobname)
  return os.path.join(jobdir, 'batch_%03d.sh' % batch_index)

def get_den_job_dir(jobname: str, calcname: str, create_if_missing: bool=True) -> str:
  calcdir = os.path.join(get_sim_job_dir(jobname), calcname)
  if create_if_missing and not os.path.exists(calcdir):
    os.mkdir(calcdir)
  return calcdir

def get_den_job_file(jobname: str, calcname: str, batch_index: int, create_if_missing: bool=True) -> str:
  return os.path.join(get_den_job_dir, 'batch_%03d.sh' % batch_index)

def get_density_param_file(jobname: str, calcname: str) -> str:
  calcdir = get_calc_dir(jobname, calcname)
  return os.path.join(calcdir, 'params.json')

def get_count_job_dir(jobname: str, calcname: str, create_if_missing: bool=True) -> str:
  calcdir = os.path.join(get_sim_job_dir(jobname), calcname)
  if create_if_missing and not os.path.exists(calcdir):
    os.mkdir(calcdir)
  return calcdir

def get_count_job_file(jobname: str, calcname: str, batch_index: int, create_if_missing: bool=True) -> str:
  return os.path.join(get_den_job_dir, 'batch_%03d.sh' % batch_index)

def get_count_param_file(jobname: str, calcname: str) -> str:
  calcdir = get_calc_dir(jobname, calcname)
  return os.path.join(calcdir, 'params.json')

def get_param_file(jobname: str) -> str:
  jobdir = get_job_dir(jobname, create_if_missing=True)
  return os.path.join(jobdir, 'run.json')

def get_position_file(jobname: str, trial_index: int) -> str:
  return os.path.join(get_job_dir(jobname), 'output_%03d.npz' % trial_index)

def get_noise_file(jobname: str, calcname: str) -> str:
  calcdir = get_calc_dir(jobname, calcname)
  return os.path.join(calcdir, 'noise.npz')

def get_noise_plot(jobname: str, calcname: str, plotname: str) -> str:
  calcdir = get_calc_dir(jobname, calcname)
  return os.path.join(calcdir, plotname)

def get_density_file(jobname: str, calcname: str, trial_index: int) -> str:
  calcdir = get_calc_dir(jobname, calcname, create_if_missing=True)
  return os.path.join(calcdir, 'density_%03d.npz' % trial_index)

def get_job_dir(jobname: str, create_if_missing: bool=False) -> str:
  jobdir = os.path.join(os.environ['RESEARCH_DATA_DIR'], 'diffusion', jobname)
  if create_if_missing and not os.path.exists(jobdir):
    os.mkdir(jobdir)
  return jobdir

def get_calc_dir(jobname: str, calcname: str, create_if_missing: bool=False) -> str:
  jobdir = get_job_dir(jobname, create_if_missing=create_if_missing)
  calcdir = os.path.join(jobdir, calcname)
  if create_if_missing and not os.path.exists(calcdir):
    os.mkdir(calcdir)
  return calcdir
