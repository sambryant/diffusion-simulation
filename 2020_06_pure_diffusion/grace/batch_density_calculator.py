import argparse
import sys
import environ
import numpy as np
from parameters import Parameters

JOBNAME = 'density test1'
PARTITION = 'scavenge'
RAM = '8000'
TIME = '12:00:00'

def go(jobname: str, calcname: str, num_x_points: int, sigma2: float, trials_per_job: int, num_trials: int) -> None:
  dparams = DensityParams(sigma2, num_x_points)
  dparams.save(jobname, calcname)

  num_batches = int(np.ceil(num_trials / trials_per_job))

  for i in range(num_batches):
    job_file = environ.get_den_job_file(jobname, calcname, i)

    f = open(job_file, 'w')
    f.write('#!/bin/bash\n')
    f.write('#SBATCH --partition="%s"\n' % PARTITION)
    f.write('#SBATCH --job-name="%s"\n' % JOBNAME)
    f.write('#SBATCH --ntasks=1 --nodes=1\n')
    f.write('#SBATCH --mem-per-cpu=%s\n' % RAM)
    f.write('#SBATCH --time="%s"\n' % TIME)
    f.write('export PYTHONPATH=/home/fas/machta/sjb94/diffusion-simulation\n')
    f.write('module load Python/3.7.0-foss-2018b\n')
    start = i * trials_per_job
    end = min(start + trials_per_job, num_trials)
    f.write('python3.7 -m "grace.density_calculator" "%s" "%s" "%d" "%d"\n' % (jobname, calcname, start, end))

if __name__ == '__main__':
  parser = argparse.ArgumentParser()
  parser.add_argument('jobname', type=str, help='Name of job')
  parser.add_argument('calcname', type=str, help='Name of calc group')
  parser.add_argument('num_x_points', type=int, help='Number of x points')
  parser.add_argument('sigma2', type=float, help='Sigma^2')
  args = parser.parse_args(sys.argv[1:])
  go(args.jobname, args.calcname, args.num_x_points, args.sigma2)
