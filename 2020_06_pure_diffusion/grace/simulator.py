import sys
import argparse
import numpy as np
import environ
import sim
from parameters import Parameters

def go(jobname: str, start_index: int, end_index: int) -> None:
  params = Parameters.load(jobname)

  for i in range(start_index, end_index):
    output = sim.simulate(params)
    output_file = environ.get_position_file(jobname, i)
    np.savez(output_file, positions=output.positions, times=output.times)

if __name__ == '__main__':
  parser = argparse.ArgumentParser()
  parser.add_argument('jobname', type=str, help='Name of job')
  parser.add_argument('st_index', type=int, help='Index of first trial')
  parser.add_argument('ed_index', type=int, help='Index of last trial')
  args = parser.parse_args(sys.argv[1:])
  go(args.jobname, args.st_index, args.ed_index)
