import numpy as np
import seaborn as sns
import sys

L = 100.0
N = 3
D = 100.0
DIMS = 2
dt = 0.01

s2D = np.sqrt(2*D)

def initialize_positions():
  return L*np.random.rand(N, DIMS)

def update_positions(pos):
  vel = s2D*np.random.normal(size=(N, DIMS))
  pos[:] += vel[:] * np.sqrt(dt)

def update_boundaries(pos):
  pos[:,0] = pos[:,0] % L
  pos[:,1] = pos[:,1] % L

def convert_trace_density(trace, ndivs, radius):
  density = np.zeros((len(trace), ndivs, ndivs))

  for i in range(len(trace)):
    density[i] = compute_density_field(trace[i], ndivs, radius)
    print(density[i])
  return density

def compute_density_field(pos, ndivs, radius):
  area = np.pi * (radius**2)
  average = area/(L*L) * N
  dx = L / ndivs
  field = np.zeros((ndivs, ndivs))
  for i in range(ndivs):
    for j in range(ndivs):
      cx = j*dx
      cy = L - i*dx
      n = count_particles(pos, np.array([cx, cy]), radius)
      field[i][j] = n - average
  return field

def count_particles(pos, center, radius):
  distance = pos[:] - center
  distance = np.linalg.norm(distance, axis=1)
  return sum(distance < radius**2)

def run_simulation(t_end, keep_trace=False):
  n_steps = int(np.ceil(t_end / dt))

  pos = initialize_positions()
  nstep = 0
  time = 0.0

  if keep_trace:
    trace = np.zeros((n_steps+1, N, DIMS))
  else:
    trace = None

  while time < t_end:
    if keep_trace:
      trace[nstep,:,:] = pos[:,:]

    update_positions(pos)
    update_boundaries(pos)

    nstep += 1
    time = nstep * dt

  if keep_trace:
    trace[nstep,:,:] = pos[:,:]

  return pos, trace

_, trace = run_simulation(10.0,True)
density = convert_trace_density(trace, 50, 1.0)
print(trace.shape)
print(density.shape)

import matplotlib.pyplot as plt
from matplotlib.animation import FuncAnimation

fig, (ax1, ax2) = plt.subplots(2, 1)
xdata, ydata = [], []
ln, = ax1.plot(trace[0,:,0], trace[0,:,1], marker='.', linewidth=0)
sns.heatmap(density[0], square=True, ax=ax2, cbar=False)

def init():
  ax1.set_aspect('equal')
  ax1.set_xlim(0, L)
  ax1.set_ylim(0, L)
  return ln,

def update(frame):
  ax2.cla()
  sns.heatmap(density[frame,:,:], square=True, cbar=False, ax=ax2)
  ln.set_xdata(trace[frame,:,0])
  ln.set_ydata(trace[frame,:,1])
  return ln,

ani = FuncAnimation(fig, update, frames=len(trace), init_func=init, blit=True)
plt.show()