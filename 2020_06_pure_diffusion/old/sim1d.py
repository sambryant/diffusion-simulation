import numpy as np
import seaborn as sns
import sys

L = 100.0
N = 3
D = 100.0
dt = 0.01
s2D = np.sqrt(2*D)

def initialize_positions():
  return L*np.random.rand(N)

def update_positions(pos):
  vel = s2D*np.random.normal(size=(N,))
  pos[:] += vel[:] * np.sqrt(dt)

def update_boundaries(pos):
  pos[:,0] = pos[:,0] % L

def compute_density(position, particles, sigma2, normalization):
  distances = (particles[:] - position)
  weights = normalization * np.exp(distances[:]*distances[:]/(2*sigma2))
  return sum(weights)

def compute_density_field(positions, particles, sigma2, normalization):
  return [
    compute_density(position, particles, sigma2, normalization)
    for position in positions
  ]

def generate_density_trace(trace, ndivs, sigma2):
  points = np.linspace(0, L, ndivs)
  normalization = 1 / (2*np.pi*sigma2)
  densities = np.zeros((len(trace), len(points)))
  for i in range(len(trace)):
    densities[i,:] = compute_density_field(points, trace[i], sigma2, normalization)
  return points, densities

# def convert_trace_density(trace, ndivs, radius):
#   density = np.zeros((len(trace), ndivs, ndivs))
#   for i in range(len(trace)):
#     density[i] = compute_density_field(trace[i], ndivs, radius)
#     print(density[i])
#   return density

# def compute_density_field(pos, ndivs, radius):
#   area = np.pi * (radius**2)
#   average = area/(L*L) * N
#   dx = L / ndivs
#   field = np.zeros((ndivs, ndivs))
#   for i in range(ndivs):
#     for j in range(ndivs):
#       cx = j*dx
#       cy = L - i*dx
#       n = count_particles(pos, np.array([cx, cy]), radius)
#       field[i][j] = n - average
#   return field

# def count_particles(pos, center, radius):
#   distance = pos[:] - center
#   distance = np.linalg.norm(distance, axis=1)
#   return sum(distance < radius**2)

def run_simulation(t_end, keep_trace=False):
  n_steps = int(np.ceil(t_end / dt))

  pos = initialize_positions()
  nstep = 0
  time = 0.0

  if keep_trace:
    trace = np.zeros((n_steps+1, N))
  else:
    trace = None

  while time < t_end:
    if keep_trace:
      trace[nstep,:,:] = pos[:,:]

    update_positions(pos)
    update_boundaries(pos)

    nstep += 1
    time = nstep * dt

  if keep_trace:
    trace[nstep,:,:] = pos[:,:]

  return pos, trace

_, trace = run_simulation(10.0,True)
points, density = generate_density_trace(trace, 10, 5)
print(trace.shape)
print(points.shape)
print(density.shape)

import matplotlib.pyplot as plt
from matplotlib.animation import FuncAnimation

fig, ax1 = plt.subplots()
ln, = ax1.plot(points, density[0], marker='.', linewidth=0.5)
ymax = max([max(counts) for counts in density])
def init():
  ax1.set_xlim(0, L)
  ax1.set_ylim(0, ymax)
  return ln,

def update(frame):
  ln.set_xdata(points)
  ln.set_ydata(density[frame])
  return ln,

ani = FuncAnimation(fig, update, frames=len(trace), init_func=init, blit=True)
plt.show()