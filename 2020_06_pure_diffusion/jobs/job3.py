import time
from parameters import Parameters
from sim_runner import Run

def main():
  jobname = 'job4'
  num_trials = 500
  num_particles = 10000
  size = 10
  t_end = 500 # Should really be like 50. See no correlations after very small transient
  dt = 0.1
  # We want each particle to traverse full space 10x
  # \< x^2 \> = 2Dt
  # So \sqrt{2Dt}/L ~ 10
  D = 100 * size / (2 * t_end) # = 1

  params = Parameters(1, size, num_particles, D, dt, t_end)
  run = Run(jobname, params, num_trials)

  t1 = time.time()
  run.go()
  t2 = time.time()
  print('Simulation took %.2f time' % (t2 - t1))

if __name__ == '__main__':
  main()
