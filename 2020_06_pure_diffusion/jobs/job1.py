import time
from parameters import Parameters
from runner import Run
from sim import simulate

def main():
  jobname = 'job1'
  num_trials = 1000
  num_particles = 1000
  size = 10
  t_end = 500
  dt = 0.1
  # We want each particle to traverse full space 10x
  # \< x^2 \> = 2Dt
  # So \sqrt{2Dt}/L ~ 10
  D = 100 * size / (2 * t_end) # = 1

  params = Parameters(1, size, num_particles, D, dt, t_end)

  t1 = time.time()
  Run(jobname, params, num_trials)
  t2 = time.time()
  print('Simulation took %.2f time' % (t2 - t1))

if __name__ == '__main__':
  main()
