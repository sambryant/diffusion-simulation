import time
from parameters import Parameters
from sim_runner import Run

def main():
  jobname = 'job6'
  num_trials = 100
  num_particles = 1000
  size = 10.0
  t_end = 5.0
  dt = 0.001
  D = 1

  params = Parameters(1, size, num_particles, D, dt, t_end, num_trials)
  run = Run(jobname, params)

  t1 = time.time()
  run.go()
  t2 = time.time()
  print('Simulation took %.2f time' % (t2 - t1))

if __name__ == '__main__':
  main()
