from typing import List

import sys, argparse
import os, json
import numpy as np
import environ
from density import DensityField
from sim_runner import Run
from parameters import Parameters
from util import print_progress_bar


class NoiseAnalysis():
  jobname: str
  calcname: str
  num_x: int
  points: np.ndarray
  x0: int
  num_t: int
  times: np.ndarray
  t0: int
  sigma2: float
  avg_density: float
  num_trials: int
  covariance_xt: np.ndarray

  def __init__(self, jobname: str, calcname: str, edge_size: float) -> None:
    self.jobname = jobname
    self.calcname = calcname

    # Extract data from run parameters
    params = Parameters.load(jobname)
    self.avg_density = params.density
    self.num_trials = params.num_trials

    # Extract data from density file
    dfield = DensityField.load(jobname, calcname, 0)
    self.points = dfield.points
    self.times = dfield.times
    self.sigma2 = dfield.sigma2

    size = params.size
    edge_fraction = edge_size / size
    self.x0 = int(edge_fraction * len(self.points))
    self.t0 = 0

    self.num_x = len(self.points) - self.x0
    self.num_t = len(self.times) - self.t0

    self.covariance_xt = np.zeros((self.num_x, self.num_t))

  def go(self) -> None:
    self.covariance_xt = np.zeros((self.num_x, self.num_t))
    for i in range(self.num_trials):
      print_progress_bar(i, self.num_trials)
      self._compute_trial(i)

    print_progress_bar(10, 10)

    self._save()

  def _save(self) -> None:
    out_file = environ.get_noise_file(self.jobname, self.calcname)
    np.savez(
      out_file,
      covariance_xt=self.covariance_xt,
      points=self.points,
      times=self.times,
      x0=self.x0,
      t0=self.t0,
      sigma2=self.sigma2,
      num_trials=self.num_trials)

  def _compute_trial(self, i: int) -> None:
    dfield = DensityField.from_file(environ.get_density_file(self.jobname, self.calcname, i))

    # Transform density field to density deviation field
    delta_rho = dfield.densities[:,:] - self.avg_density

    del_rho_1 = delta_rho[self.t0, self.x0]

    for t in range(self.num_t):
      for x in range(self.num_x):
        del_rho_2 = delta_rho[self.t0 + t, self.x0 + x]
        self.covariance_xt[x, t] += (del_rho_1 * del_rho_2) / self.num_trials


# def go(jobname, edge_size, use_time_average):
#   output_dir = environ.get_data_dir(jobname)
#   run_file =
#   num_trials = run.num_trials

#   # Open first file to get some metadata
#   density_file = os.path.join(output_dir, 'density_%02d.npz' % 0)
#   dfield = DensityField.from_file(density_file)


# jobname = sys.argv[1]

# output_dir = environ.get_data_dir(jobname)
# run_file = os.path.join(output_dir, 'run.json')
# run = Run.from_json(json.loads(open(run_file, 'r').read()))
# num_trials = run.num_trials

# density_file = os.path.join(output_dir, 'density_%02d.npz' % 0)
# dfield = DensityField.from_file(density_file)
# points = dfield.points
# times = dfield.times
# sigma2 = dfield.sigma2

# x0 = int(len(points)/4)
# t0 = 10

# num_x = len(points) - x0 - 1
# num_t = len(times) - t0 - 1

# sxt = np.zeros((num_x, num_t)) # structure factor

# counter = 0
# for i in range(num_trials):
#   print_progress_bar(i, num_trials)

#   density_file = os.path.join(output_dir, 'density_%02d.npz' % i)
#   if not os.path.exists(density_file):
#     print('warning: file not found: %s' % density_file)
#     continue

#   dfield = DensityField.from_file(density_file)
#   drho1 = dfield.densities[t0, x0] - run.params.density

#   drhoT = 0.0

#   for t in range(num_t):
#     for x in range(num_x):
#       drho2 = dfield.densities[t0+t, x0+x] - run.params.density
#       drhoT += drho2
#       sxt[x,t] += drho1 * drho2
#   counter += 1
#   drhoT /= (num_x * num_t)

# print_progress_bar(num_trials, num_trials)

# # Take average
# for t in range(num_t):
#   for x in range(num_x):
#     sxt[x,t] /= counter

# noise_file = os.path.join(output_dir, 'noise.npz')
# np.savez(
#   noise_file,
#   sxt=sxt,
#   x0=x0,
#   t0=t0,
#   points=points,
#   times=times,
#   sigma2=sigma2)





if __name__ == '__main__':
  parser = argparse.ArgumentParser()
  parser.add_argument(
    'jobname',
    type=str,
    help='Name of job')
  parser.add_argument(
    'calcname',
    type=str,
    help='Name of calculation within job group')
  parser.add_argument(
    'edge_size',
    type=float,
    help='X position for fixed point of C(x,t) calculation, should be far enough away from the edge')
  args = parser.parse_args(sys.argv[1:])
  na = NoiseAnalysis(args.jobname, args.calcname, args.edge_size)
  na.go()
