from __future__ import annotations
import os
import json
import numpy as np
import environ
from parameters import Parameters, Output
from sim import simulate
from util import print_progress_bar


class Run(object):
  jobname: str
  params: Parameters

  def __init__(self, jobname: str, params: Parameters) -> None:
    self.jobname = jobname
    self.params = params

  def go(self) -> None:
    self.params.save(self.jobname)

    print('Running %d trials with parameters:' % self.params.num_trials)
    print(self.params)

    for i in range(self.params.num_trials):
      print_progress_bar(i, self.params.num_trials)
      pos_file = environ.get_position_file(self.jobname, i)
      if os.path.exists(pos_file):
        print('Skipping existing file: %s' % pos_file)
        continue
      output = simulate(self.params)
      np.savez(pos_file, positions=output.positions, times=output.times)
    print_progress_bar(self.params.num_trials, self.params.num_trials)




